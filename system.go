
package tdwlUtil
import (
	"bytes"
	"errors"
	"fmt"
	"os/exec"
	"strings"
)

func GetServicePidWithSupervisor()(string,error){
	cmd := exec.Command("supervisorctl", "status")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return "",err
	}
	for {
		line, err := out.ReadString('\n')
		if err != nil {
			break
		}
		if strings.Contains(line,"TDSlave")||strings.Contains(line,"zookeeper") ||strings.Contains(line,"cron")||strings.Contains(line,"unix:"){
		continue
		}else {
			tokens := strings.Split(line, " ")
			ft := make([]string, 0)
			for _, t := range tokens {
				if t != "" && t != "\t" {
					t=strings.Replace(t,",","",-1)
					ft = append(ft, t)
				}
			}
			return ft[3],nil
		}
	}
	return "",errors.New("pid not find")
}
func GetCpuUtilizationWithPid(pid string)(string,error){
	if pid==""{
		return "",errors.New("invalid PID")
	}
	cmd := exec.Command("top", "-b","-n","1")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		fmt.Println("Run error")
	}
	for {
		line, err := out.ReadString('\n')
		if err != nil {
		break
		}
		tokens := strings.Split(line, " ")
		ft := make([]string, 0)
		for _, t := range tokens {
			if t != "" && t != "\t" {
				ft = append(ft, t)
				fmt.Println(t)
			}
		}

		if ft[0]==pid {
			fmt.Println(ft[0],pid,ft[8])
			return ft[8],nil
		}
	}
	return "",errors.New("CpuUtilization not find")
}
